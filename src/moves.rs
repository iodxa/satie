/// this will take information about the current state of the board from fen and return with a single chosen move
use std::{process::Command,
          time::SystemTime};

use chess::{Board,
            ChessMove,
            Color,
            Game,
            GameResult,
            MoveGen,
            Piece,
            Rank,
            Square,
            ALL_SQUARES};

use std::collections::HashMap;
use indexmap::IndexMap;


fn printmoves(moves: &IndexMap<ChessMove, f64>, movestrings: &IndexMap<ChessMove, String>) {
    //let iter = &moves.sort_by()
    for (m, ev) in moves {
        eprintln!("{}: {} || {}", m.to_string(), 
        {
            if -ev >= 100000.0 {
                "#".to_string()
            } else {
                (-ev).to_string()
            }
        }
        , movestrings[m]);
    }
}

fn highest_eval(moves: IndexMap<ChessMove, f64>, colour: Color) -> (ChessMove, f64) {
    let (m, e) = match colour {
        //Color::White => moves.iter().max_by(|a, b| a.1.partial_cmp(&b.1).unwrap()).unwrap(),
        Color::White => moves.iter().min_by(|b, a| b.1.partial_cmp(&a.1).unwrap()).unwrap(),
        Color::Black => moves.iter().min_by(|b, a| b.1.partial_cmp(&a.1).unwrap()).unwrap()
    };
    (*m, *e)
}

fn sort_moves(moves: &mut IndexMap<ChessMove, f64>, colour: Color) {
    match colour {
        //Color::White => moves.sort_by(|_, a, _, b| b.partial_cmp(&a).unwrap()),
        Color::White => moves.sort_by(|_, a, _, b| a.partial_cmp(&b).unwrap()),
        Color::Black => moves.sort_by(|_, a, _, b| a.partial_cmp(&b).unwrap())
    }
}

/// takes input of a chess::Board and returns a single move
pub fn bestmove(
    game: Game, depth: i32, movetime: Option<u128>, singlethreaded: bool, play: Option<String>
) -> Result<String, chess::Error> {
    use std::str::FromStr;

    eprintln!("{}", game.current_position());

    let board = game.current_position();
    let timer = SystemTime::now();
    //let board = Board::default();

    //search(board, 2).to_string()

    let mut cmd = Command::new("./db.rb");

    let command = cmd.arg(board.to_string()).output().expect("heck");
    let status = command.status;
    if status.success() {
        //println!("returning a move:");
        Ok(String::from_utf8_lossy(&command.stdout).to_string())
    } else {
        eprintln!("could not get a move from db.rb");

        let mut moves = match play {
            Some(p) => match ChessMove::from_str(p.as_str()) {
                Ok(m) => {
                    let mut t = IndexMap::<ChessMove, f64>::new();
                    t.insert(m, 0.0);
                    t
                },
                Err(e) => {
                    panic!("invalid move for -play, uci format (e.g. e2e4): {}", e)
                }
            },
            None => first_guess(board, None)
        };

        sort_moves(&mut moves, game.side_to_move());

        //printmoves(moves.clone());
        if depth != 0 {
            //println!("running singlethreaded");
            let (mut moves, movestrings) = search(game.clone(), depth, None, moves, None);
            let (bestmove, _) = highest_eval(moves.clone(), game.side_to_move());
            sort_moves(&mut moves, game.side_to_move());
            printmoves(&moves, &movestrings);
            return Ok(bestmove.to_string());
        }
        let mut depth = 1;
        //eprintln!("depth: {}", depth);
        //println!("could not get a move from database looking for a new move:");
        //Ok(search(game, depth, None).to_string());

        // iterative deepening

        let (mut moves, mut movestrings) = search(
            game.clone(),
            depth,
            None,
            moves.clone(),
            Some(timer.elapsed().unwrap().as_millis()),
        );


        while timer.elapsed().unwrap().as_millis() < movetime.unwrap() {
            sort_moves(&mut moves, game.side_to_move());
            printmoves(&moves, &movestrings);
            depth += 1;
            let (temp2, temp) = search(
                game.clone(),
                depth,
                None,
                moves.clone(),
                Some(movetime.unwrap() - timer.elapsed().unwrap().as_millis()),
            );
            movestrings = temp;
            moves = temp2;
            /*
            println!(
                "depth: {}, moves: {:?}\ntime left: {}\ncontinue? {}",
                depth,
                moves,
                movetime.unwrap() - timer.elapsed().unwrap().as_millis(),
                timer.elapsed().unwrap().as_millis() < movetime.unwrap()
            );
            */
            //let (bestmove, eval) = highest_eval(moves.clone(), game.side_to_move());
            //if (eval >= ((depth - 1) * 1000000) as f64 && game.side_to_move() == Color::Black) || (eval <= ((depth - 1) * -1000000) as f64 && game.side_to_move() == Color::White) {
                //eprintln!("{:?} forced mate: {} > {}", game.side_to_move(), eval, ((depth - 1) * 100000) as f64);
                //return Ok(bestmove.to_string())
            //}
        }
        sort_moves(&mut moves, game.side_to_move());
        printmoves(&moves, &movestrings);
        let (bestmove, _) = highest_eval(moves, game.side_to_move());
        Ok(bestmove.to_string())
    }
    //println!("bestmove fen: {}", board.to_string());
    //Ok(search(board, 3).to_string())
}

fn first_guess(board: Board, pvmove: Option<ChessMove>) -> IndexMap<ChessMove, f64> {
    let mut moves = vec![];
    //let mut moves = IndexMap::new();

    let mut iterable = MoveGen::new_legal(&board);
    for m in &mut iterable {
        let mut eval = 0.0;
        if let Some(pv) = pvmove {
            if pv == m {
                eval += 100.0;
            }
        }
        // this line panics sometimes, figure out why later
        eval += (80 * board.make_move_new(m).checkers().popcnt()) as f64;
        eval += match (board.piece_on(m.get_source()), board.piece_on(m.get_dest())) {
            (Some(x), Some(y)) => piece_val(y) - piece_val(x) + 8.0,
            (..) => 0.0,
        };
        /*
        eval += match board.piece_on(m.get_source()) {
            Some(x) => 0.0,
            _ => 0.0,
        };
        */
        eval += match m.get_promotion() {
            Some(Piece::Queen) => 20.0,
            _ => 0.0,
        };

        moves.push((m, eval))
    }
    //moves.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
    moves.into_iter().collect()
}

use std::thread;
use std::sync::mpsc;
use std::time::Duration;
/// return the best move
pub fn search(
    game: Game, depth: i32, pvmove: Option<ChessMove>, mut moves: IndexMap<ChessMove, f64>, movetime: Option<u128>,
) -> (IndexMap<ChessMove, f64>, IndexMap<ChessMove, String>) {
    println!("searching depth {}", depth);
    let time = SystemTime::now();
    let mut threads = vec![];

    //let board = game.current_position();

    //let (mut bestmove, _) = highest_eval(moves.clone(), game.side_to_move());

    let mut movestrings = IndexMap::<ChessMove, String>::new();

    let (send, recv) = mpsc::channel();
    for (m, _) in moves.clone() {
        //let board = game.current_position();
        //let g = Game::new_with_board(board.make_move_new(m));
        movestrings.insert(m, "".to_string());
        let mut g = game.clone();
        g.make_move(m);
        let s = send.clone();
        let t = thread::spawn(move || {
            //let ev = minimax(g, depth - 1, f64::NEG_INFINITY, f64::INFINITY);
            let ev = negamax(g, f64::NEG_INFINITY, f64::INFINITY, depth - 1);
            //let ev = negamax_no_pruning(g, depth - 1);
            s.send((m, ev)).unwrap();
        });
        threads.push(t);
    }


    let mut count = 0;
    for thread in threads {
        if let Some(t) = movetime {
            if time.elapsed().unwrap().as_millis() >= t {
                return (moves, movestrings)
            }
        }
        //let (m, (ev, x)) = thread.join().unwrap();
        let (m, (ev, x, movestring)) = if let Some(t) = movetime {
            match recv.recv_timeout(Duration::from_millis(t as u64)) {
                Ok(epic) => epic,
                Err(e) => {
                    println!("positions: {}\n{}", count, e);
                    return (moves, movestrings)
                }
            }
        } else {
            thread.join().unwrap();
            recv.recv().unwrap()
        };
        eprintln!("{}: {}: {}: {:?}", depth, m, -ev, game.side_to_move());
        moves.insert(m, ev);
        movestrings.insert(m, movestring);
        //println!("{}: {}", m.to_string(), ev);
        count += x;
        //println!("{} {}: {}", e, m, ev);
    }
    println!("positions: {}", count);

    printmoves(&moves, &movestrings);
    println!("===============================");
    (moves, movestrings)
}

use min_max::*;
pub fn negamax(game: Game, mut alpha: f64, beta: f64, depth: i32) -> (f64, usize, String) {
    let board = game.current_position();
    match game.result() {
        Some(GameResult::WhiteCheckmates) => return ((100000 * depth) as f64, 1, format!("#{}", depth)),
        Some(GameResult::BlackCheckmates) => return ((-100000 * depth) as f64, 1, format!("-#{}", depth)),
        Some(GameResult::Stalemate) => return (0.0, 1, "-".to_string()),
        _ => (),
    };
    if game.can_declare_draw() {
        return (0.0, 1, "-".to_string());
    }
    if depth <= 0 {
        if is_quiescent(board) {
            let ev = position(board);
            return (ev, 1, format!("quiescent: {}", ev))
        } else {
            let (ev, x) = quiesce(game, alpha, beta, 3);
            return (ev, x, "~~~~~~~".to_string())
        }
    }
    let mut count = 1;
    let moves = first_guess(board, None);
    let mut bestmove = "".to_string();
    for (m, _) in moves {
        let mut g = game.clone();
        g.make_move(m);
        let (ev, x, mut movestring) = negamax(g, -beta, -alpha, depth - 1);
        count += x;
        movestring = format!("{} => {}", m.to_string(), movestring);
        if -ev > alpha {
            alpha = -ev;
            bestmove = movestring.clone();
        }
        if -ev >= beta {
            return (beta, count, movestring)
        }
        alpha = max_partial!(alpha, -ev);
    }
    (alpha, count, bestmove)
}

pub fn negamax_no_pruning(game: Game, depth: i32) -> (f64, usize) {
    let board = game.current_position();
    match game.result() {
        Some(GameResult::WhiteCheckmates) => return ((-100000 * depth) as f64, 1),
        Some(GameResult::BlackCheckmates) => return ((100000 * depth) as f64, 1),
        Some(GameResult::Stalemate) => return (0.0, 1),
        _ => (),
    };
    if depth <= 0 {
        return (position(board), 1)
    }
    let mut eval = f64::NEG_INFINITY;
    let mut count = 1;
    let moves = first_guess(board, None);
    for (m, _) in moves {
        let mut g = game.clone();
        g.make_move(m);
        let (ev, x) = negamax_no_pruning(g, depth - 1);
        count += x;
        eval = max_partial!(eval, -ev);
    }
    (eval, count)
}

fn quiesce(game: Game, mut alpha: f64, beta: f64, depth: i32) -> (f64, usize) {
    let board = game.current_position();
    let stand_pat = match game.side_to_move() {
        Color::White => position(board),
        Color::Black => -position(board),
    };

    match game.result() {
        Some(GameResult::WhiteCheckmates) => return ((-100000 * depth) as f64, 1),
        Some(GameResult::BlackCheckmates) => return ((100000 * depth) as f64, 1),
        Some(GameResult::Stalemate) => return (0.0, 1),
        _ => (),
    };

    if depth <= 0 {
        return (stand_pat, 1)
    }
    
    if alpha < stand_pat {
        alpha = stand_pat;
    }

    let mut is_quiescent = true;

    let mut count = 1;
    let mut moves = first_guess(board, None);
    sort_moves(&mut moves, game.side_to_move());
    //if depth >= 7 {
    //    eprintln!("\n\n");
    //    printmoves(moves.clone());
    //    eprintln!("\n\n");
    //}
    for (m, _) in moves.clone() {
        if board.piece_on(m.get_dest()).is_some() 
        || board.checkers().popcnt() > 0 
        || num_checkers(board, m) > 0 {
            is_quiescent = false;
            if value_on(board, m.get_dest()) - see(&game, m.get_dest()) < 0 {
                eprintln!("see < 0");
                break;
            }
            let mut g = game.clone(); 
            g.make_move(m);
            let (ev, x) = quiesce(g, -beta, -alpha, depth - 1);
            count += x;
            //eprintln!("{} || {}:    {}, alpha: {}, beta:, {}", depth, m.to_string(), -ev, alpha, beta);
            alpha = max_partial!(alpha, -ev);
            if -ev >= beta {
                return (beta, count);
            }
            
            //eprintln!("  -| {}:    {}, alpha: {}, beta:, {}", m.to_string(), ev, alpha, beta);
        }
    }
    if is_quiescent {
        //println!("{}\nquiescent position: {}, {}", board.to_string(), stand_pat, alpha);
        //return (stand_pat, 1)
    }
    (alpha, count)
}
/*
int see(int square, int side)
{
   value = 0;
   piece = get_smallest_attacker(square, side);
   /* skip if the square isn't attacked anymore by this side */
   if ( piece )
   {
      make_capture(piece, square);
      /* Do not consider captures if they lose material, therefor max zero */
      value = max (0, piece_just_captured() -see(square, other(side)) );
      undo_capture(piece, square);
   }
   return value;
}
*/

/// returns the value of the piece on sq
fn value_on(board: Board, sq: Square) -> i32 {
    match board.piece_on(sq) {
        Some(Piece::Pawn) => 1,
        Some(Piece::Bishop) => 3,
        Some(Piece::Knight) => 3,
        Some(Piece::Rook) => 5,
        Some(Piece::Queen) => 9,
        _ => 0,
    }
}

/// static exchange evaluation
fn see(game: &Game, sq: Square) -> i32 {
    let board = game.current_position();
    let mut value = 0;
    let piece = match board.piece_on(sq) {
        Some(Piece::Pawn) => 1,
        Some(Piece::Bishop) => 3,
        Some(Piece::Knight) => 3,
        Some(Piece::Rook) => 5,
        Some(Piece::Queen) => 9,
        _ => return 0,
    };
    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if m.get_dest() == sq {
            let mut g = game.clone();
            g.make_move(m);
            value = max_partial!(value, piece - see(&g, sq));
        }
    }
    //eprintln!("{}", lowest_value);
    value
}

/// take a given position & return an advantage as f64
pub fn minimax(game: Game, depth: i32, mut alpha: f64, mut beta: f64) -> (f64, usize) {
    //let game = Game::new_with_board(board);
    let board = game.current_position();
    //println!("evaluating: {}", board.to_string());
    match game.result() {
        Some(GameResult::WhiteCheckmates) => return ((100000 * depth) as f64, 1),
        Some(GameResult::BlackCheckmates) => return ((-100000 * depth) as f64, 1),
        Some(GameResult::Stalemate) => return (0.0, 1),
        _ => (),
    };

    if game.can_declare_draw() {
        return (0.0, 1)
    }

    if depth <= 0 {
        //let (ev, x) = quiescent(game.clone(), alpha, beta, 0, None);
        //let (ev, x) = quiescent(game.clone(), alpha, beta, 0, None);

        //eprintln!("quiescent({}): {}", game.current_position().to_string(), -ev);
        let ev = position(board);
        //println!("swapped: {:?}", match game.side_to_move() {
        //    Color::White => (ev, x),
        //    Color::Black => (-ev, x)
        //});
        //return (ev, x);
        //return(position(board), 1);
        //return dbg!(quiescent(game, f64::NEG_INFINITY, f64::INFINITY))
        return (ev, 1)
    }

    let mut eval = match game.side_to_move() {
        Color::White => f64::NEG_INFINITY,
        Color::Black => f64::INFINITY,
    };


    let mut count = 1;
    let moves = first_guess(board, None);
    for (m, _) in moves {
        let mut g = game.clone();
        g.make_move(m);
        //println!("{}:", m.to_string());
        let (ev, x) = minimax(g, depth - 1, alpha, beta);
        count += x;
        match game.side_to_move() {
            // maximise
            Color::White => {
                eval = max_partial!(ev, eval);
                alpha = max_partial!(ev, alpha);
                if beta <= alpha {
                    return (beta, count)
                }
            }
            // minimise
            Color::Black => {
                eval = min_partial!(ev, eval);
                beta = min_partial!(ev, beta);
                if beta <= alpha {
                    return (beta, count)
                }
            }
        }
    }
    (eval, count)
}


/*
QUIESCE(Iower, alpha) integer beta, upper;
{ 
    integer null;
    makenuU; null ~ - evaluate(-alpha,-beta); unmakenull;
    foreach move m do
    { if( null >= alpha) return(bestv);
    make(m); v <-- - QUIESCE(-alpha, -null); unmake(m);
    if( v > null ) bestv <-- v;
    }
    return(null); 
}
*/

fn combine(mut a: IndexMap<ChessMove, f64>, b: Option<IndexMap<ChessMove, f64>>) -> IndexMap<ChessMove, f64> {
    match b {
        Some(x) => {
            for (m, e) in x {
                a.insert(m, e);
            }
            a.clone()},
        None => a.clone(),
    }
}

fn num_checkers(board: Board, m: ChessMove) -> u32 {
    let mut result = Board::default();
    board.make_move(m, &mut result);
    result.checkers().popcnt()
}

fn quiesced(game: Game, lower: f64, upper: f64, depth: i32) -> (f64, f64, f64) {
    let board = game.current_position();
    if depth <= 0 {
        return (f64::NEG_INFINITY, position(board), f64::INFINITY)
    }

    let mut lower2 = f64::NEG_INFINITY;
    let mut upper2 = f64::NEG_INFINITY;

    // make a null move
    let mut bestv = match board.null_move() {
        Some(b) => position(b),
        None => f64::INFINITY
    };

    bestv = -bestv;

    if bestv > lower {
        lower2 = bestv;
    }

    //println!("{:?}", (lower2, bestv, upper2));
    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if board.piece_on(m.get_dest()).is_some() 
        || num_checkers(board, m) < board.checkers().popcnt() {
            let mut g = game.clone(); 
            g.make_move(m);
            let (lm, vm, um) = quiesced(g, -upper, -bestv, depth - 1);
            let l = -um;
            let v = -vm;
            let u = -lm;
            if l > lower2 {
                lower2 = l;
            }
            if v > bestv {
                bestv = v;
            }
            if u > upper2 {
                upper2 = u;
            }
        }
    }
    (lower2, bestv, upper2)
}

fn quiesce2(game: Game, lower: f64, upper: f64) -> (f64, usize) {
    let board = game.current_position();
    
    let (mut bestv, mut count) = match board.null_move() {
        Some(b) => quiesce1(Game::new_with_board(b), -upper, -lower),
        None => (f64::NEG_INFINITY, 1)
    };

    if is_quiescent(board) {
        //eprintln!("quiescent position, epic {}\n{}", null, board.to_string());
        return (bestv, 1)
    }

    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if board.piece_on(m.get_dest()).is_some() 
        || num_checkers(board, m) < board.checkers().popcnt() {
            if bestv >= upper {
                return (bestv, count)
            }
            let mut g = game.clone(); 
            g.make_move(m);
            let (mut v, x) = quiesce2(g, -upper, -bestv);
            v = -v;
            count += x;
            if v > bestv {
                bestv = v;
            }
        }
    }
    (bestv, count)
}
fn quiesce1(game: Game, lower: f64, upper: f64) -> (f64, usize) {
    let board = game.current_position();
    let mut bestv = match board.null_move() {
        Some(b) => position(b),
        None => f64::NEG_INFINITY
    };

    bestv = -bestv;
    let mut count = 1;

    if is_quiescent(board) {
        //eprintln!("quiescent position, epic {}\n{}", null, board.to_string());
        return (bestv, 1)
    }

    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if board.piece_on(m.get_dest()).is_some() 
        || num_checkers(board, m) < board.checkers().popcnt() {
            if bestv >= upper {
                return (bestv, count)
            }
            let mut g = game.clone(); 
            g.make_move(m);
            let (mut v, x) = quiesce2(g, -upper, -bestv);
            v = -v;
            count += x;
            if v > bestv {
                bestv = v;
            }
        }
    }
    (bestv, count)
}

fn null(game: Game) -> f64 {
    let board = game.current_position();
    let mut eval = match game.side_to_move() {
        Color::White => f64::INFINITY,
        Color::Black => -187.5,
    };
    let stand_pat = position(board);
    if is_quiescent(board) {
        eprintln!("quiescent position, epic {}\n{}", stand_pat, board.to_string());
        return stand_pat
    }
    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if board.piece_on(m.get_dest()).is_some() {
            let mut g = game.clone(); 
            g.make_move(m);
            let ev = position(g.current_position());
            eval = match game.side_to_move() {
                Color::White => min_partial!(ev, eval),
                Color::Black => max_partial!(ev, eval),
            };
        }
    }
    eval
}

fn quiescent(game: Game, mut alpha: f64, mut beta: f64, depth: i32, premoves: Option<IndexMap<ChessMove, f64>>) -> (f64, usize) {
    let board = game.current_position();
    let stand_pat = position(board);

    if depth <= 0 {
        return (stand_pat, 1)
    }

    if is_quiescent(board) {
        eprintln!("{}\nquiescent position: {}", board.to_string(), stand_pat);
        return (stand_pat, 1)
    }

    let mut null = match board.null_move() {
        Some(b) => position(b),
        None => stand_pat
    };

    /*
    if null > 0.0 {
        null = -null
    }
    */

    //eprintln!("{} : {}", alpha, beta);

    if null >= beta {
        return (beta, 1);
    } 
    if alpha < null {
        alpha = null;
    }

    //eprintln!("{} || null: {}", depth, null);
    if board.checkers().popcnt() > 0 {
        null = match game.side_to_move() {
            Color::White => alpha,
            Color::Black => beta,
        }
    }

    //let mut iterable = MoveGen::new_legal(&board);
    let mut count = 1;
    let moves = first_guess(board, None);
    for (m, _) in moves.clone() {
        if match premoves.clone() { 
            Some(pre) => board.piece_on(m.get_dest()).is_some() && !pre.contains_key(&m),
            None => board.piece_on(m.get_dest()).is_some()
        } || board.checkers().popcnt() > 0 {
            let mut g = game.clone(); 
            g.make_move(m);
            let (ev, x) = quiescent(g, alpha, beta, depth - 1, Some(combine(moves.clone(), premoves.clone())));
            eprintln!("{} || {}:    {}, alpha: {}, beta:, {}", depth, m.to_string(), ev, alpha, beta);
            count += x;
            match game.side_to_move() {
                // maximise
                Color::White => {
                    alpha = max_partial!(ev, alpha);
                    if beta <= alpha {
                        return (beta, 1)
                    }
                    null = max_partial!(ev, null);
                    //println!("{}: {}: null: {}, ev: {}, alpha: {}, beta: {}, colour: {:?}", depth, m.to_string(), null, ev, alpha, beta, game.side_to_move());
                }
                // minimise
                Color::Black => {
                    beta = min_partial!(ev, beta);
                    if beta <= alpha {
                        return (beta, 1)
                    }
                    null = min_partial!(ev, null);
                    if board.checkers().popcnt() > 0 {
                        null = beta
                    }
                    //println!("{}: {}: null: {}, ev: {}, alpha: {}, beta: {}", depth, m.to_string(), null, ev, alpha, beta);
                }
            }
            /*
            if ev > null && game.side_to_move() == Color::White {
                null = ev;
            }
            if ev < null && game.side_to_move() == Color::Black {
                null = ev;
            }
            */
            //println!("{}, {}: {:?}", depth, m.to_string(), board.piece_on(m.get_dest()));
            //let (ev, x) = quiescent(g, -alpha, -beta);
            //eprintln!("{} || {} : {}", depth, alpha, beta);
        } 
    }
    //eprintln!("{} || {} evaluated: {}\nnull: {}", depth, board.to_string(), stand_pat, null);
    (null, count)
}

/// returns position difference of a given board
fn position(board: Board) -> f64 {
    let mut total = 0.0;
    for sq in ALL_SQUARES.iter() {
         match board.color_on(*sq) {
            Some(Color::White) => total += match board.piece_on(*sq) {
                Some(Piece::Pawn) => 1.0,
                Some(Piece::Bishop) => 3.25,
                Some(Piece::Knight) => 3.0,
                Some(Piece::Rook) => 5.0,
                Some(Piece::Queen) => 9.0,
                _ => 0.0,
            },
            Some(Color::Black) => total -= match board.piece_on(*sq) {
                Some(Piece::Pawn) => 1.0,
                Some(Piece::Bishop) => 3.25,
                Some(Piece::Knight) => 3.0,
                Some(Piece::Rook) => 5.0,
                Some(Piece::Queen) => 9.0,
                _ => 0.0,
            },
            _ => (),
        }
        match board.piece_on(*sq) {
            // avoid unnecessary king moves
            Some(Piece::King) => match board.side_to_move() {
                Color::White => total -= 0.0,
                Color::Black => total += 0.0,
            },
            // avoid minor pieces on the back rank
            Some(Piece::Knight) | Some(Piece::Bishop) => {
                match (board.color_on(*sq), sq.get_rank()) {
                    (Some(Color::Black), Rank::Eighth) => total += 0.5,
                    (Some(Color::White), Rank::First) => total -= 0.5,
                    _ => (),
                }
            }
            _ => (),
        }
    }
    total
}

fn piece_val(piece: Piece) -> f64 {
    match piece {
        Piece::Pawn => 1.0,
        Piece::Knight => 3.0,
        Piece::Bishop => 3.0,
        Piece::Rook => 5.0,
        Piece::Queen => 9.0,
        Piece::King => 0.0,
    }
}

fn is_quiescent(board: Board) -> bool {
    let mut iterable = MoveGen::new_legal(&board);
    for m in &mut iterable {
        if board.piece_on(m.get_dest()).is_some() || board.checkers().popcnt() != 0 {
            return false;
        }
    }
    true
}

/// return the best move but its singlethreaded
/*
pub fn search_singlethread(
    game: Game, depth: i32, pvmove: Option<ChessMove>, _movetime: Option<u128>,
) -> ChessMove {
    println!("searching depth {}", depth);
    //let time = SystemTime::now();

    let board = game.current_position();
    let mut eval = match game.side_to_move() {
        Color::White => f64::NEG_INFINITY,
        Color::Black => f64::INFINITY,
    };
    let mut alpha = f64::NEG_INFINITY;
    let mut beta = f64::INFINITY;
    let mut count = 0;
    let moves = sort_moves(board, pvmove);
    let mut ctm = moves[0].0;
    for (m, _) in moves {
        let mut g = game.clone();
        g.make_move(m);
        let (ev, x) = evaluate(g, depth - 1, f64::NEG_INFINITY, f64::INFINITY);
        count += x;
        //println!("{} {}: {}", e, m, ev);
        println!("{}: {}", m.to_string(), ev);
        match game.side_to_move() {
            // maximise
            Color::White => {
                if ev >= 99999.0 * (depth - 1) as f64 {
                    return m
                };
                //eval = max_partial!(ev, eval);
                if ev > eval {
                    ctm = m;
                    eval = ev;
                }
                alpha = max_partial!(ev, alpha);
                if beta <= alpha {
                    break
                }
            }
            // minimise
            Color::Black => {
                if ev <= -99999.0 * (depth - 1) as f64 {
                    return m
                };
                //eval = min_partial!(ev, eval);
                if ev < eval {
                    ctm = m;
                    eval = ev;
                }
                beta = min_partial!(ev, beta);
                if beta <= alpha {
                    break
                }
            }
        }
    }
    println!("positions: {}", count);
    ctm
}
*/

#[cfg(test)]
mod tests {
    use chess::{Board,
                MoveGen};
    use std::str::FromStr;

    use super::*;
    /// recursive function that takes input of a board, and plays all moves
    fn test_moves(board: Board, d: i32) -> i32 {
        if d < 0 {
            return 0
        }
        let mut count = 0;
        let mut iterable = MoveGen::new_legal(&board);
        let mut t = 0;
        for m in &mut iterable {
            t += test_moves(board.make_move_new(m), d - 1);
            count += 1;
        }
        if t > 0 {
            return t
        }
        count
    }
    #[test]
    fn move_number_test0() {
        let board = Board::default();
        assert_eq!(test_moves(board, 0), 20);
    }
    #[test]
    fn move_number_test1() {
        let board = Board::default();
        assert_eq!(test_moves(board, 1), 400);
    }
    #[test]
    fn move_number_test2() {
        let board = Board::default();
        assert_eq!(test_moves(board, 2), 8902);
    }
    #[test]
    fn move_number_test3() {
        let board = Board::default();
        assert_eq!(test_moves(board, 3), 197281);
    }
    #[test]
    fn position_default() {
        assert_eq!(position(Board::default()), 0.0);
    }
    #[test]
    fn quiescent() {
        let not_quiet = Board::from_str("rn1qkb1r/ppp2ppp/3p1n2/4pb2/4P3/2P2N2/PP1PBPPP/RNBQK2R w KQkq - 1 5").unwrap();
        let quiet = Board::from_str("rn1qkb1r/ppp2ppp/3p1n2/4pP2/8/2P2N2/PP1PBPPP/RNBQK2R b KQkq - 0 5").unwrap();

        assert_eq!(is_quiescent(quiet), true);
        assert_eq!(is_quiescent(not_quiet), false);
    }
    #[test]
    fn white_advantage() {
        let advantage = Board::from_str("r1bq1r2/pp2bppk/5n2/3p4/3Q4/2N3N1/PPP2PPP/R1B2RK1 b - - 0 1").unwrap();
        eprintln!("{}", position(advantage));
        assert!(position(advantage) > 0.0);
    }
    #[test]
    fn checkers() {
        let board = Board::from_str("8/p1r2pkp/1p2p3/3pP2q/3P4/P3P1P1/1P3Q2/5RK1 w - - 0 25").unwrap();
        //assert_eq!(num_checkers)
    }
    #[test]
    fn static_exchange_evaluation() {
        let board = Board::from_str("k2r4/8/5n2/3p4/8/2N5/8/K6B w - - 0 1").unwrap();
        let mut game = Game::new_with_board(board);
        let play = ChessMove::from_str("h1d5").unwrap();
        game.make_move(play);
        assert_eq!(-2, 1 - see(&game, play.get_dest()))
    }
    #[test]
    fn static_exchange_evaluation_2() {
        let board = Board::from_str("k1b5/3q4/8/5N2/6P1/8/8/K7 b - - 0 1").unwrap();
        let mut game = Game::new_with_board(board);
        let play = ChessMove::from_str("d7f5").unwrap();
        game.make_move(play);
        assert_eq!(-5, value_on(board, play.get_dest()) - see(&game, play.get_dest()))
    }
    #[test]
    fn static_exchange_evaluation_3() {
        let board = Board::from_str("r1bq1rk1/pppp2b1/2n2npp/4ppB1/2P5/2NP1NP1/PP2PPBP/1R1Q1RK1 w - - 0 10").unwrap();
        let mut game = Game::new_with_board(board);
        let play = ChessMove::from_str("f3e5").unwrap();
        game.make_move(play);
        assert_eq!(-2, value_on(board, play.get_dest()) - see(&game, play.get_dest()))
    }
}

/*
/// uses a neural net to return a chosen move given a position
fn nn() -> String {
    unimplemented!()
}
*/
