/* http://wbec-ridderkerk.nl/html/UCIProtocol.html

  Description of the universal chess interface (UCI)    April 2004
  ================================================================

* The specification is independent of the operating system. For Windows,
the engine is a normal exe file, either a console or "real" windows application.

* all communication is done via standard input and output with text commands,

* The engine should boot and wait for input from the GUI,
the engine should wait for the "isready" or "setoption" command to set up its internal parameters
as the boot process should be as quick as possible.

* the engine must always be able to process input from stdin, even while thinking.

* all command strings the engine receives will end with '\n',
also all commands the GUI receives should end with '\n',
Note: '\n' can be 0x0c or 0x0a0c or any combination depending on your OS.
If you use Engine und GUI in the same OS this should be no problem if you cummunicate in text mode,
but be aware of this when for example running a Linux engine in a Windows GUI.

* The engine will always be in forced mode which means it should never start calculating
or pondering without receiving a "go" command first.

* Before the engine is asked to search on a position, there will always be a position command
to tell the engine about the current position.

* by default all the opening book handling is done by the GUI,
but there is an option for the engine to use its own book ("OwnBook" option, see below)

* if the engine or the GUI receives an unknown command or token it should just ignore it and try to
parse the rest of the string.

* if the engine receives a command which is not supposed to come, for example "stop" when the engine is
not calculating, it should also just ignore it.

\\ cass' note to self: follow everything here to the freaking letter //
*/

use std::{fs,
          fs::File,
          io,
          io::prelude::*,
          path::Path,
          str::FromStr};

use chess::{Board,
            ChessMove,
            Color,
            Game};
use structopt::StructOpt;

use crate::moves::*;
/// A very slow and stupid chess engine
#[derive(StructOpt, Debug)]
#[structopt(name = "ctengine")]
struct Opt {
    /// Start from a given fen
    #[structopt(
        short,
        long,
        default_value = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    )]
    fen: String,

    /// Set the depth of the evaluation
    #[structopt(short, long, default_value = "0")]
    depth: i32,

    /// Start processing bestmove
    #[structopt(short, long)]
    go: bool,

    /// Activate debug mode
    #[structopt(long)]
    debug: bool,

    /// amount of time for a move
    #[structopt(short, long, default_value = "0")]
    time: u128,

    /// search with a single thread
    #[structopt(short, long)]
    single_threaded: bool,

    /// search a specific move
    #[structopt(short, long)]
    play: Option<String>,
    
}

fn append(str: &str) -> Option<io::Result<()>> {
    let mut file = fs::OpenOptions::new()
        .write(true)
        .append(true)
        .open("enginestuff.txt")
        .unwrap();

    Some(file.write_all(str.as_bytes()))
}

fn stdout(str: &str) -> Option<io::Result<()>> {
    //write!(file, "{}", str)?;
    append(str);
    Some(io::stdout().write_all(str.as_bytes()))
}

pub fn start() {
    let mut game = Game::new();

    let opt = Opt::from_args();
    if opt.go {
        let split = opt.fen.as_str().split(' ');

        let mut cmd: Vec<&str> = split.collect();
        let mut v = vec!["position"];
        v.append(&mut cmd);
        position(&mut game, v);
        match bestmove(
            (game).clone(),
            opt.depth,
            Some(opt.time),
            opt.single_threaded,
            opt.play
        ) {
            Ok(m) => eprintln!("bestmove {}", m),
            Err(err) => panic!("could not find a move: {}", err),
        }
        return
    }

    let path = Path::new("enginestuff.txt");
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let _ = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why),
        Ok(file) => file,
    };

    loop {
        let io = read_line_buffer();
        let split = io.as_str().split(' ');

        let cmd: Vec<&str> = split.collect();

        append("input: ");
        append(&io.to_owned());
        append("\n");
        //eprintln!("{}", io);
        // use option type here
        match cmd[0] {
            "uci" => uciout(),
            "isready" => stdout("readyok\n"),
            "ucinewgame" => ucinewgame(),
            "position" => position(&mut game, cmd),
            "go" => go(&game, cmd),
            "stop" => None,
            "quit" => return,
            //"evaluate" => eval(game.current_position()),
            //"position" => mat(game.current_position()),
            _ => None,
        };
        //eprintln!("{}", board.to_string());
    }
}

/*
fn mat(board: Board) -> Option<io::Result<()>> {
    eprintln!("position difference: {}", position(board));
    stdout(&(position(board).to_string())[..])
}

fn eval(board: Board) -> Option<io::Result<()>> {
    let e = evaluate(board, 5, Color::White);
    eprintln!("evaluation: {}", e);
    stdout(&(e.to_string())[..])
}
*/

fn uciout() -> Option<io::Result<()>> {
    stdout(
        "id name CTEngine 0.1.0\nid author Izzy Cassell\noption name Move Overhead type spin \
         default 10 min 0 max 5000\noption name Debug Log File type string default\noption name \
         Threads type spin default 1 min 1 max 512\noption name Hash type spin default 16 min 1 \
         max 33554432\noption name Clear Hash type button\noption name Ponder type check default \
         false\noption name MultiPV type spin default 1 min 1 max 500\noption name Skill Level \
         type spin default 20 min 0 max 20\noption name Move Overhead type spin default 10 min 0 \
         max 5000\noption name Slow Mover type spin default 100 min 10 max 1000\noption name \
         nodestime type spin default 0 min 0 max 10000\noption name UCI_Chess960 type check \
         default false\noption name UCI_AnalyseMode type check default false\noption name \
         UCI_LimitStrength type check default false\noption name UCI_Elo type spin default 1350 \
         min 1350 max 2850\noption name UCI_ShowWDL type check default false\noption name \
         SyzygyPath type string default <empty>\noption name SyzygyProbeDepth type spin default 1 \
         min 1 max 100\noption name Syzygy50MoveRule type check default true\noption name \
         SyzygyProbeLimit type spin default 7 min 0 max 7\noption name Use NNUE type check \
         default true\noption name EvalFile type string default nn-735bba95dec0.nnue\nuciok\n",
    )
}

fn go(game: &Game, cmd: Vec<&str>) -> Option<io::Result<()>> {
    use min_max::*;
    //eprintln!("{}", board.to_string());
    //bestmove();
    let mut wtime: u128 = 0;
    let mut btime: u128 = 0;
    let mut winc: u128 = 0;
    let mut binc: u128 = 0;
    for i in 0..cmd.len() {
        match cmd[i] {
            "wtime" => wtime = cmd[i + 1].parse().unwrap(),
            "btime" => btime = cmd[i + 1].parse().unwrap(),
            "winc" => winc = cmd[i + 1].parse().unwrap(),
            "binc" => binc = cmd[i + 1].parse().unwrap(),
            &_ => (),
        }
    }
    let str = match game.side_to_move() {
        Color::White => {
            let time = min!(wtime / 10 + winc, 20000);
            match bestmove((*game).clone(), 0, Some(time), false, None) {
                Ok(str) => str,
                Err(err) => panic!("something fucked up in the bestmove() function: {}", err),
            }
        }
        Color::Black => {
            let time = min!(btime / 10 + binc / 3, 20000);
            match bestmove((*game).clone(), 0, Some(time), false, None) {
                Ok(str) => str,
                Err(err) => panic!("something fucked up in the bestmove() function: {}", err),
            }
        }
    };

    let str = String::from("bestmove ") + &str + &String::from("\n");

    stdout(&str[..])
}

fn position(game: &mut Game, v: Vec<&str>) -> Option<io::Result<()>> {
    //eprintln!("{}", v[1]);

    //let board = &game.current_position();
    *game = Game::new_with_board(match v[1] {
        "startpos" => Board::default(),
        _ => {
            let fen = v[1..7].to_vec();
            let mut fens = "".to_string();
            for v in fen {
                fens += v;
                fens += " ";
                //eprintln!("{}", fens)
            }
            match Board::from_str(fens.as_str()) {
                Ok(board) => board,
                Err(err) => panic!("some bullshit: {}", err),
            }
        }
    });

    let mut moves = v[1..v.len()].to_vec();
    let mut count = 0;
    for k in &v {
        count += 1;
        //eprintln!("k: {}, count: {}, v.len(): {}", k, count, v.len());
        if *k == "moves" {
            moves = v[count..v.len()].to_vec();
            break
        }
    }
    //eprintln!("{:?}", v);
    if count == v.len() {
        return None
    }
    for m in moves {
        //eprintln!("{}", m);
        //let temp = *board;
        let chessmove = ChessMove::from_str(m);
        //eprintln!("{:?}", chessmove);
        match chessmove {
            Ok(x) => (*game).make_move(x),
            Err(err) => panic!("could not play {}, {}", m, err),
        };
    }
    eprintln!("position:  {}", (*game).current_position().to_string());
    //eprintln!("fen: {}", board.to_string());
    //if let Position { board } = pos {}
    None
}

fn ucinewgame() -> Option<io::Result<()>> {
    None
}

/// input loop to take input & return output to stdout
fn read_line_iter() -> String {
    let stdin = io::stdin();
    // Read one line of input iterator-style
    let input = stdin.lock().lines().next();
    input
        .expect("No lines in buffer")
        .expect("Failed to read line")
        .trim()
        .to_string()
}

fn read_line_buffer() -> String {
    // Read one line of input buffer-style
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");
    input.trim().to_string()
}

pub fn uci() {
    stdout("start uci");
    read_line_iter();
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn position_test() {
        let split = "position startpos moves e2e4 c7c5".split(' ');

        let cmd: Vec<&str> = split.collect();
        let mut game = Game::new();
        position(&mut game, cmd);

        let board =
            match Board::from_str("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2") {
                Ok(board) => board,
                Err(err) => panic!("well fuck, your position function doesnt function: {}", err),
            };
        assert_eq!(board, game.current_position(),)
    }
    #[test]
    fn game_from_board() {
        let _gamestr =
            match Game::from_str("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2") {
                Ok(board) => board,
                Err(err) => panic!("well fuck, your position function doesnt function: {}", err),
            };

        let mut board = Board::default();
        let mut game = Game::new();
        let m = match ChessMove::from_str("e2e4") {
            Ok(x) => x,
            Err(err) => panic!("fuck: {}", err),
        };
        game.make_move(m);
        board = board.make_move_new(m);
        let m = match ChessMove::from_str("c7c5") {
            Ok(x) => x,
            Err(err) => panic!("fuck: {}", err),
        };
        game.make_move(m);
        board = board.make_move_new(m);

        assert_eq!(game.current_position(), board)
    }
}
